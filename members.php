<!DOCTYPE html>
<html>
<head>
<style>
body {
	background-color: #f4eee7;
}

div {
	position: relative;
}

div.select {
	background-color: lightgrey;
	margin-right: 150px;
}

div.pics {
	top: 10px;
}

h1 {
	text-align: center;
}

p {
	text-align: center;
	font-family: "Times New Roman";
	font-size: 20px;
}

table {
	position: relative;
	left: 200px;
	width: 50%;
}

div.desc {
	text-align: center;
	font-weight: normal;
	width: 120px;
	margin: 5px;
}

div.menu {
	float: left;
	margin: 10px;
	background-color: silver;
}

form {
	float: left;
	margin-right: 100%;
}

div.container {
	
}

section {
	
}

section.head {
	
}

section.input {
	
}

section.body {
	
}

div.nav {
	margin: 15px;
}

div.foot { //
	position: absolute;
	float: right; //
	top: 100%;
	width: 130px;
	height: 60px;
}

section.footer { //
	float: left;
	position: relative;
	top: 100%;
	height: 62px;
	width: 100%;
	background-color: #071E3D;
	z-index: 99;
}

#menu {
	width: 100%;
	float: left;
	margin: 0 0 1em 0;
	padding: 0;
	background-color: #012c51;
	border-bottom: 3px solid #097abb;
	border-top: 3px solid #097abb;
}

#menu ul {
	list-style-type: none;
	width: 800px;
	margin: 0 auto;
	padding: 0;
	box-sizing: inherit;
}

#menu li {
	box-sizing: inherit;
	float: left;
}

#menu li a {
	display: block;
	padding: 8px 15px; /*10px 50px 10px 50px;*/
	text-decoration: none;
	font-weight: bold;
	color: #E6E6FA;
	border-right: 1px solid #ccc;
}

#menu li:first-child a {
	border-left: 1px solid #ccc;
}

#menu li a:hover {
	background-color: #097abb;
	color: #012c51;
}

img.icon {
	height: 34px;
}

section.head {
	
}

#ucmlogo {
	border-radius: 25px;
	width: 100%;
	height: 200px;
	z-index: 999;
	background-image: url(imgs/cover.jpg);
	background-repeat: no-repeat;
	background-size: cover;
}

.myWrapper {
	max-width: 660px;
	margin: 0 0;
}

.logo {
	display: inline-block;
	width: 50px;
	height: 20px;
	margin-top: 5px;
	background-size: auto 50px;
	background-repeat: no-repeat;
}

#ucmlogo {
	position: relative;
	border-radius: 25px;
	left: 7.5%;
	width: 85%;
	height: 360px;
	z-index: 999; //
	background-image: url(imgs/cover.jpg); //
	background-repeat: no-repeat; //
	background-size: cover;
}

img.cover {
	border-radius: 25px;
	width: 100%;
	height: 370px;
	z-index: 999;
}

.myWrapper {
	max-width: 660px;
	margin: 0 0;
}

.logo {
	display: inline-block;
	width: 50px;
	height: 20px;
	max-width: 50px;
	margin-top: 5px;
	background-size: auto 50px;
	background-repeat: no-repeat;
}

img.blumcenter {
	position: static;
	width: 120px;
	height: 60px;
	margin-right: 10px;
}

img.ucmlogo {
	position: absolute;
	width: 50px;
	height: 50px;
	top: 0px;
	left: 0px;
}

section.bluetop {
	background-color: rgb(1, 44, 81);
}
</style>
</head>

<body>

	<section class="bluetop">
		<nav>
			<a href="http://www.ucmerced.edu"> <img class="ucmlogo"
				src="imgs/University_of_California_-_Merced_UCM_609739_i0.jpg"
				align="left">
			</a>
		</nav>
		<div id="ucmlogo">
			<img class="cover" src="/imgs/cover.jpg">
			<div class="myWrapper"></div>
		</div>

		<section class="container">
			<br>
			<section class="menu">
				<div id="menu">
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><a href="image_archives.php">Image Archives</a></li>
						<li><a href="research.html">Research</a></li>
						<li><a href="members.php">Inside UCMVis</a></li>
						<li><a href="gallery.php">Gallery</a></li>
					</ul>
				</div>
			</section>
		</section>
	</section>

	<section class="head">
		<h1>University of California, Merced Web Camera</h1>
	</section>

	<p>Hello, you have found yourself at a developing page.</p>
	<p>
		Dr. Shawn Newsam is the founder of this project, which started around
		March of 2015. He started through his own funding as well as a grant
		from Blum Center for developing economies. For more information about
		Dr. Newsam, please visit his <a
			href="http://faculty.ucmerced.edu/snewsam/index.html"> faculty page</a>.
	</p>
	<p>Landon and Ai-Linh are undergraduate students who were hired for a
		multitude of tasks ranging from building the website, the web-server,
		configuring the camera system, and working with faculty members to
		complete the setup process.</p>
	<p></p>

	<p>Thank you for visiting!</p>



	<section class="footer">
		<div class="foot">
			<a target="_self" href="http://blumcenter.ucmerced.edu/"> <img
				class="blumcenter" src="imgs/blum-logo-preview.jpg" alt="BLUM logo">
			</a>
		</div>
	</section>

</body>
</html>
