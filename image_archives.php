<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<head>

  <script type="text/javascript" src="datetimepicker_css.js">
  ////Date Time Picker script- by TengYong Ng of http://www.rainforestnet.com
  //Script featured on JavaScript Kit (http://www.javascriptkit.com)
  //For this script, visit http://www.javascriptkit.com
  </script>

  <script
  src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"
  charset="utf-8"></script>

  <link href ="index_style.css" rel="stylesheet" type="text/css" />

  <style>
  #body{
	  background-color: #f4eee7;
  }
  #calgif{
    display:inline;
  }
  </style>


  <link href="src/css/lightbox.css" rel="stylesheet">
  <link href ="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel ="stylesheet"
  integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
  crossorigin="anonymous">
</head>

<body id="body">
  <div class="container">
    <div id="page-header" class="row">

      <nav>
        <a href="http://www.ucmerced.edu"> <img class="ucmlogo"
          src="imgs/University_of_California_-_Merced_UCM_609739_i0.jpg"
          align="left">
        </a>
      </nav>
    </div>

    <div id="image-menu-top" class ="container">
      <img id="cover" class ="cover" src="/imgs/cover--.jpg">
    </div>
    <div class ="row myMenuRow">
      <div class ="col-xs-1 myMenuElement"><a href ="index.php"><span>Home</span></a></div>
      <div class ="col-xs-1 myMenuElement"><a href ="image_archives.php">Image Archives</a></div>
      <div class ="col-xs-1 myMenuElement"><a href ="research.php">Research</a></div>
      <div class ="col-xs-1 myMenuElement"><a href ="members.php">Inside UCMVis</a></div>
      <div class ="col-xs-1 myMenuElement"><a href ="gallery.php">Gallery</a></div>
    </div>

	<?php
	$_SESSION["date"] = date("Y/m/d");
	?>

    <div>
      <form  method="get" >
        <input readonly="readonly"  name="demo1" type="Text" id="demo1" maxlength="25" size="25" value='<?php if(isset($_SESSION["date"])) echo $_SESSION["date"];?>'/>
        <img id ="calgif" src="cal.gif" onclick="javascript:NewCssCal('demo1','yyyymmdd')" style="cursor:pointer" />
        <?php
		$date = date("Y/m/d");
        if( htmlspecialchars(isset($_GET["demo1"])))
        {
          $date = $_GET["demo1"];
		  $_SESSION["date"] = $date;
        }
        
        //echo $date;
        ?>
        <input type='submit' name='submitday' value='Get day'  />
      </form>
    </div>

	
    <div class="row">
        <p>Selected day pictures: </p>
		<p>Click on an image to zoom, then try the new gallery function! </p>
	</div>	
	
	
        <div class="pics row">
          <?php
		  
		  
          if(!($date)){
			$date = date ( "Y/m/d" );
            $files = glob ( "Pictures/" . date ( "Y/m/d" ) . "/*.JPG" );
          }
          //else the date has been picked, we should use that
          $files = glob ( "Pictures/" . $date . "/*.JPG");

          //files should always be set, even if the directory is empty
          if (!isset($files)){
            echo $date;
            echo "Your directory is not set! <br>";
            echo "Getting default! <br>";
            $date = date ( "Y/m/d" ) ;
            echo $date  . "<br>";
            $files = glob ( "Pictures/" . $date . "/*.JPG");
          }
          foreach ( $files as $filename ) {
            
            //for each file, get gif thumbnail if possible
            $fileGif = str_replace(".JPG", ".gif", $filename);
            
			//put each img into its own column (3 cols)
			echo "<div class='col-xs-4'>" ;
			
			//find gif if exists
			
			//originally displays gif, but links to the full file. 
			if(isset($fileGif)){
				echo "<A href='$filename' data-lightbox='collection' data-title='Use arrow keys or click the right half of the image to move forward.'>";
				echo " <img class='responsive' src= '$fileGif' alt='gif does not exist for this picture'> ";
            }
            else{
				echo "<A href='$filename' data-lightbox='collection' data-title='Use arrow keys or click the right half of the image to move forward.'>";
				echo " <img class='responsive' src= '$filename' alt='bad filepath' width=50 height = 50 > ";
			}
			echo "</a>";
			echo "</div>" ;
		  }
          ?>
		</div>
	</div>

  <script src="src/js/lightbox.js"></script>
  
</body>
</html>
