<!DOCTYPE html>
<html>
<head>
<style>

body {
    background-color: #f4eee7;
}

div{
	position:relative;
}
div.select{
	background-color: lightgrey;
	margin-right: 150px;
}
div.pics
{
	top: 10px;
}

h1 {
	text-align: center;
}

p {
	text-align: center;
    font-family: "Times New Roman";
    font-size: 20px;
}

table{
		position:relative;
		left:200px;
		width:50%;
}

div.desc {
    text-align: center;
    font-weight: normal;
    width: 120px;
    margin: 5px;
}
div.menu{
	float:left;
	margin:10px;
	background-color:silver;
}
form
{
	float:left;
	margin-right: 100%;
}
div.container
{

}
section
{

}
section.head
{

}
section.input
{


}
section.body{


}
div.nav
{
	margin:15px;
}

#menu{
        width: 100%;
        float: left;
        margin: 0 0 1em 0;
        padding: 0;
        background-color:  #012c51;
        border-bottom: 3px solid #097abb;
        border-top: 3px solid #097abb;
    }
#menu ul{
   list-style-type:  none;
   width: 800px;
   margin: 0 auto;
   padding: 0;
   box-sizing: inherit;
}
#menu li {
	box-sizing: inherit;
    float: left;   
}

#menu li a{
  display:  block;
  padding: 8px 15px; /*10px 50px 10px 50px;*/
  text-decoration: none;
  font-weight: bold;
  color: #E6E6FA;
  border-right: 1px solid #ccc;
}
    
 #menu li:first-child a{
        border-left:  1px solid #ccc;
    }

#menu li a:hover{
      background-color: #097abb;
      color:  #012c51;
}
img.icon{
		height:34px;
}

#ucmlogo{
  border-radius: 25px;
  width:  100%;
  height: 200px;
  z-index: 999;
  background-image: url(imgs/cover.jpg);  
  background-repeat: no-repeat;
  background-size: cover;
}
.myWrapper{
    max-width: 660px;
    margin: 0 0;    
}
    
.logo{
   
   display: inline-block;
   width: 50px;
   height: 20px;
   margin-top: 5px;
   background-size: auto 50px;
   background-repeat: no-repeat;
}

img.icon{
		height:34px;
}
    
#ucmlogo{
  position:relative;
  border-radius: 25px;
  left:7.5%;
  width:  85%;
  height: 360px;
  z-index: 999;
  //background-image: url(imgs/cover.jpg); 
  //background-repeat: no-repeat;
  //background-size: cover;
}
img.cover
{
  border-radius: 25px;
  width:  100%;
  height: 370px;
  z-index: 999;

}
.myWrapper{
    max-width: 660px;
    margin: 0 0;    
}
    
.logo{
   display: inline-block;
   width: 50px;
   height: 20px;
   max-width:50px;
   margin-top: 5px;
   background-size: auto 50px;
   background-repeat: no-repeat;
}

img.ucmlogo
{
	position:absolute;
   width: 50px;
   height: 50px;
   top:0px;
   left:0px;
}
img.responsive
{
	max-width: 48%;
	height: auto;
}


section.bluetop{
	background-color:rgb(1, 44, 81);
}

</style>
</head>

<body> 
	
  <section class="bluetop">
	 <nav>
               <a href="http://www.ucmerced.edu">
			   <img class="ucmlogo" src="imgs/University_of_California_-_Merced_UCM_609739_i0.jpg" align="left">
			   </a>
            </nav>
	<div id="ucmlogo">
	<img class="cover" src="/imgs/cover.jpg" > 
      
    </div>
	
	<section class="container">
    <br>
	<section class="menu">
    <div id="menu">
      <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="image_archives.php">Image Archives</a></li>
        <li><a href="research.html">Research</a></li>
        <li><a href="members.php">Inside UCMVis</a></li>
		<li><a href="gallery.php">Gallery</a></li>
      </ul>
    </div>
    </section>	
	</section>
	</section>	
	
	<?php  include 'dropdown.php'; ?>
	
		<section class="input">
			<!-- Dropdown of Years followed by button  -->
			<form method="post" id="dateform">
				<div class="select">
				
					<?php
					$nameYear = 'my_year';
					$curr = getcwd(); //current directory of server /var/www/html/image_archives.php
					$scanned_directory = array_diff(scandir("Pictures/" , SCANDIR_SORT_DESCENDING), array('..', '.')); //root of directory tree "Year"
					$yearOpts = $scanned_directory; // yearOpts = "Year/"; yearOpts is an array
					//$selected = 0;
					//if( isset($_POST['submityear'])
					$selectedYr = $_POST[$nameYear]; //?? maybe select default value - may delete
					echo dropdown( $nameYear, $yearOpts, $selectedYr );	//make dropdown	
					//echo "selectedyr: " . $yearOpts[$selectedYr]; 
					// figure out a way to get rid of button so it can populate upon click of dropdown
					echo " <input type='submit' name='submityear' value='Select Year' /> ";	//make button
					?>
					<p></p>
					
					<?php
						$nameMonth = 'this_month';
						//echo "TEST:" ;
														//i need selected year here
						if($yearOpts[$selectedYr]) //not null array	
						{
							$scanned_directory = array_diff(scandir("Pictures/" . $yearOpts[$selectedYr], SCANDIR_SORT_DESCENDING), array('..', '.'));
						}
						else
						{
							$scanned_directory = array_diff(scandir("Pictures/" . date("Y/"), SCANDIR_SORT_DESCENDING), array('..', '.'));
						}
						$monthOptions= $scanned_directory; //monthOptions array
						//$selectedmonth = 0;
						//save month to variable...
						$selectedMonth = $_POST[$nameMonth];	//remember their selection
						echo dropdown( $nameMonth, $monthOptions, $selectedMonth );//set as default
					echo " <input type='submit' name='submitmonth' value='Get month' /> ";
					?>
					<p></p>
					<?php
					$nameday = 'my_day'; //name of form
					if($yearOpts[$selectedYr]) // 
					{
					$d_dir = array_diff( scandir("Pictures/" . $yearOpts[$selectedYr] . "/". $monthOptions[$selectedMonth] ,SCANDIR_SORT_DESCENDING ), array('..','.'));
					}
					else
					{
					$d_dir = array_diff(scandir("Pictures/" . date("Y/m/"), SCANDIR_SORT_DESCENDING), array('..', '.'));
					}
					$dayOptions= $d_dir;
					//$selected = 0;
					$selected_val = $_POST[$nameday];
					echo dropdown( $nameday, $dayOptions, $selected_val );
					//if( ! isset($_POST['submityear']) || !isset($_POST['submitmonth']))
					echo " <input type='submit' name='submitday' value='Get day'  /> ";
					//if(isset($_POST['submitday']))
					{
						//$selected_val = $_POST[$nameday];  // Storing Selected Value In Variable
						//$selected_year = $_POST[$nameYear];
						//echo "You have selected day: " .$dayOptions[$selected_val];  // Displaying Selected Value
					}
				?>
				</div>
			</form>
		</section>
		
		<section class = "body">
			<p> Selected day pictures: </p>
		<div class= "pics">	
			
		<?php
			if($yearOpts[$selectedYr])
			{
				$files = glob( "Pictures/" . $yearOpts[$selectedYr] . "/". $monthOptions[$selectedMonth]  .  "/"  .$dayOptions[$selected_val] . "/*.JPG");
			}
			else
			{
				$files = glob( "Pictures/" . date("Y/m/d") . "/*.JPG");
			}
			if ( ! $files)
				echo "There are no pictures yet";
			foreach( $files as $filename){
				//echo $filename ;
				echo "<A href='$filename'>";
				//echo "<img class='responsive' src= '$filename'  alt=' bad image path?' width=50 height = 50> ";
				echo "$filename";
				echo "</a>";
				echo "<br>";
				}
		?>
		</div>
		<section class = "body">
		
	</div>
</body>
</html>
