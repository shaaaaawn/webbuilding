<!DOCTYPE html>
<html>

<head>
  <title> Home Page </title>
  <meta http-equiv ="X-UA-Compatible" content="IE =EmulateIE7">
  <meta charset="UTF-8">
  
  <script
  src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"
  charset="utf-8"></script>
  <script
  src="monkeecreate-jquery.simpleWeather-0d95e82/jquery.simpleWeather.js"
  charset="utf-8"></script>
  <script type ="text/javascript">
  $(document).ready(function () {
    $.simpleWeather({
      location: 'Merced, CA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        html = '<pre> Merced Weather Forecast ';
        html += ' Current Temperature: ' + weather.temp + '&deg;' + weather.units.temp;
        html += ' Condition: ' + weather.text;
        html += ' Wind Speed: ' + weather.wind.speed + ' mph, ' + weather.wind.direction;
        html += ' Pressure: ' + weather.pressure;
        html += ' Humidity: ' + weather.humidity;
        html += ' Heat Index: ' + weather.heatindex;
        html += ' Visibility Index: ' + weather.visibility + '</pre>';


        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  });
  </script>
  <link rel="prefetch" href ="image_archives.php">

  <link href ="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel ="stylesheet"
  integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
  crossorigin="anonymous">

  <link href ="index_style.css" rel="stylesheet" type="text/css" />
</head>

<body>
  <div class ="container-fluid">
  
		<div id="page-header" class="row">
		  <marquee behavior="scroll" direction ="left" scrollamount="12"
		  style="float: right">
			<div id="weather" ></div>
			</marquee>
		
		<nav>
		  <a href="http://www.ucmerced.edu"> <img class="ucmlogo"
			src="imgs/University_of_California_-_Merced_UCM_609739_i0.jpg" align="left">
		  </a>
		</nav>
		
		
			
		</div>
		
		

  <div id="image-menu-top" class = "row" >

	  
	  
	  
   <img id="cover" style="width:491.467px; height:360px; max-width: 491.467px;max-height:360px;margin:0px auto;" class ="cover" src="imgs/cover--.jpg"></img>

	  
  </div>
  
  
  <div class ="row myMenuRow">
    <div class ="col-xs-1 myMenuElement"><a href ="index.php"><span>Home</span></a></div>
    <div class ="col-xs-1 myMenuElement"><a href ="image_archives.php">Image Archives</a></div>
    <div class ="col-xs-1 myMenuElement"><a href ="research.php"><span> Research</span></a></div>
    <div class ="col-xs-1 myMenuElement"><a href ="members.php">Inside UCMVis</a></div>
    <div class ="col-xs-1 myMenuElement"><a href ="gallery.php"><span> Gallery</span></a></div>
  </div>

  <div class="row" style="display:table; margin: 0 auto;">
		  
		  <div style = "min-width:164px; float:left;" class ="col-xs-2 purplee" >
		  <p>
			If you are a member of the IEEE,
			<a href ="http://ieeexplore.ieee.org/xpl/articleDetails.jsp?tp=&arnumber=5711556&searchWithin%3Dp_First_Names%3AShawn%26searchWithin%3Dp_Last_Names%3ANewsam%26matchBoolean%3Dtrue%26queryText%3D%28p_Authors%3ANewsam%2C+Shawn%29"> here</a> is a link to the latest publication.
		  </p>
		  <p>
			Todays air quality index is found at <a
			href ="http://www.airnow.gov/index.cfm?action=airnow.local_city&cityid=283">
			www.airnow.gov </a>
		  </p>
		  <p>
			For an archive of our images, click <a href ="image_archives.php">
			  here </a>
			</p>
      </div>
	  
		
		
		
    <div style = "float:left" class="content col-xs-5">

      <div >
        <h2>Today's Image</h2>
        Servertime: <?php echo date('g:i:s A'); ?>.
        <br> Please be patient for this image to update on its 15 minute
        marks.
      </div>
      <div><br>
        <?php include('index_NEWEST.php'); ?>

        <div >
          <h2>Yesterday's Image</h2> <br>
          <?php include('index_pickDays.php'); ?>
        </div>
      </div>

    </div>
	
	<div id="section1" class="content col-xs-4" style="float:left; max-height:377.6px">
        <p>UC Merced brings you live images of the Sierra Nevada Mountains.</p>
        <p>
          Currently, we are set up above our <a
          href ="imgs/ucmerced_campusmap.jpg">Science and Engineering Building
          1</a>. There is one camera that takes pictures every 15 minutes.
        </p>
        <p>This page provides the latest image with a comparison of the day
          before, and extra information with relevant links.</p>
        </div>
  </div>

    
      

      <?php include('index_scrape.php'); ?>

      <!-- </section> -->
      <div id="pageScrape" class="row ">
        <div id="scrapedInfo" class="col-xs-12 content">
          <?php

          echo 'Air quality data brought to you by valleyair.org <br>';
          echo '<a href= "http://www.valleyair.org/snswebpage/default.aspx?x=Coffee">
          www.valleyair.org/snswebpage/default.aspx?x =Coffee
          </a>';

          echo $quote, PHP_EOL;
          ?>

        </div>
      </div>
   

</div>

    <section class ="footer">
      <div class ="foot">
        <a target="_self" href="http://blumcenter.ucmerced.edu/"> <img
          class="blumcenter" src="imgs/blum-logo-preview.jpg" alt ="BLUM logo">
        </a>
      </div>
    </section>
	
  </body>
  </html>
