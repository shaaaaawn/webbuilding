#!/bin/bash

HOME_DIRS="/var/www/html/Pictures"
NEWEST_DIR="/var/www/html/NEWEST"
YEAR_DIR=$(date '+%Y')
MONTH_DIR=$(date '+%m')
DAY_DIR=$(date '+%d')


for FOLDER in $HOME_DIRS; do
	cp $NEWEST_DIR/*.JPG $FOLDER/$YEAR_DIR/$MONTH_DIR/$DAY_DIR
done
