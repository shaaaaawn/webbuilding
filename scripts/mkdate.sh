#/bin/bash

HOME_DIRS="/var/www/html/Pictures"
YEAR_DIR=$(date '+%Y')
MONTH_DIR=$(date '+%m')
DAY_DIR=$(date '+%d')

for FOLDER in $HOME_DIRS; do
	mkdir -p "${FOLDER}/${YEAR_DIR}/${MONTH_DIR}/${DAY_DIR}"
done

for FOLDER in $HOME_DIRS; do
	chmod 777 $FOLDER/$YEAR_DIR/$MONTH_DIR/$DAY_DIR
	chmod 777 $FOLDER/$YEAR_DIR/$MONTH_DIR
	chmod 777 $FOLDER/$YEAR_DIR
done
